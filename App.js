import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import { MapView, Camera, Permissions, Location, Constants } from 'expo';
export default class App extends React.Component {
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
    position: {
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421
    }
  };

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
    const { statusLocation } = await Permissions.askAsync(Permissions.LOCATION);
    this.setState({hasLocationPermission: statusLocation === 'granted' })
    
    
  }


  sacaFoto = () =>{
    let location = Location.getCurrentPositionAsync().then(location => {
      this.setState( {position: { latitude: location.coords.latitude, longitude: location.coords.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 }});
    });
  }

  render() {
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Camera style={{ flex: 1 }} type={this.state.type}>
            <View
              style={{
                flex: 1,
                backgroundColor: 'transparent',
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                style={{
                  flex: 0.1,
                  alignSelf: 'flex-end',
                  alignItems: 'center',
                }}
                onPress={() => {
                  this.setState({
                    type: this.state.type === Camera.Constants.Type.back
                      ? Camera.Constants.Type.front
                      : Camera.Constants.Type.back,
                  });
                }}>
                <Text
                  style={{ fontSize: 18, marginBottom: 10, color: 'white' }}>
                  {' '}Flip{' '}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  flex: 0.5,
                  alignSelf: 'flex-end',
                  alignItems: 'center',
                }}
                onPress={() => {
                  this.sacaFoto();
                }}>
                <Text
                  style={{ fontSize: 18, marginBottom: 10, color: 'white'}}>
                  {' '}Photo{' '}
                </Text>
              </TouchableOpacity>

            </View>
          </Camera>

          <MapView
            style={{ flex: 1 }}
            region={this.state.position}
            initialRegion={{
              latitude: this.state.position.latitude,
              longitude: this.state.position.longitude,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          />
          
        </View>
      );
    }
  }
}
